import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
     
      "& > *": {
        margin: "auto",
      },
    },

    large: {
      width: theme.spacing(10),
      height: theme.spacing(10),
    },
    typoContainer : {
        paddingTop :'10%',
        paddingBottom :'10%'
    },
    typo : {
        fontWeight : 'bold'
    }
    
    
  })
);

export default function ProfilCard() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Avatar alt="Aymen Abidi" src="photo.PNG" className={classes.large} />
      <div className={classes.typoContainer}>
      <Typography className={classes.typo} variant='body1' color='textSecondary'>Mohamed Aymen Abidi</Typography>
      <Typography variant='body2' color='textSecondary'>Full stack developer</Typography>
      </div>
     
    </div>
  );
}
