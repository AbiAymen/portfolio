import React from "react";
import { motion } from "framer-motion";
import { Typography } from "@material-ui/core";

const container = {
  hidden: { opacity: 1, scale: 0 },
  visible: {
    opacity: 1,
    scale: 1,
    transition: {
      delay: 0.3,
      when: "beforeChildren",
      staggerChildren: 0.1,
    },
  },
};

const item = {
  hidden: { y: 20, opacity: 0 },
  visible: {
    y: 0,
    opacity: 1,
  },
};

export default function Bg() {
  return (
    <div>
      <div style={{ display: "flex", justifyContent: "flex-start" }}>
        <motion.div
          className="container"
          variants={container}
          initial="hidden"
          animate="visible"
        >
          <Typography variant="h1" color="textPrimary">
            Skills
          </Typography>
        </motion.div>
      </div>
      <div style={{ display: "flex", justifyContent: "flex-start" }}>
        <motion.ul
          className="container"
          variants={container}
          initial="hidden"
          animate="visible"
        >
          <motion.div className="item" variants={item}>
            <Typography variant="h5" color="textPrimary">
              React
            </Typography>
          </motion.div>
          <motion.div className="item" variants={item}>
            <Typography variant="h5" color="textPrimary">
              Angular
            </Typography>
          </motion.div>
          <motion.div className="item" variants={item}>
            <Typography variant="h5" color="textPrimary">
              ASP.NET
            </Typography>
          </motion.div>
          <motion.div className="item" variants={item}>
            <Typography variant="h5" color="textPrimary">
              C#
            </Typography>
          </motion.div>
          <motion.div className="item" variants={item}>
            <Typography variant="h5" color="textPrimary">
              Javascript
            </Typography>
          </motion.div>
          <motion.div className="item" variants={item}>
            <Typography variant="h5" color="textPrimary">
              Typescript
            </Typography>
          </motion.div>
          <motion.div className="item" variants={item}>
            <Typography variant="h5" color="textPrimary">
              SQL
            </Typography>
          </motion.div>
        </motion.ul>
      </div>
    </div>
  );
}
