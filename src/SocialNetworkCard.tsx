import React from 'react';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import FacebookIcon from '@material-ui/icons/Facebook';
import { makeStyles, createStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    row: {
        width: "100%",
        display: "flex",
        justifyContent: "flex-start",
      },
  
      rowContentText: {
        color: "white",
        marginTop: "auto",
        marginBottom: "auto",
        justifyContent: "left",
        marginLeft: "18%",
      },
      rowContentIcon: {
        color: "white",
        marginTop: "auto",
        marginBottom: "auto",
        marginLeft: "15%",
        justifyContent: "left",
        fontWeight: 'bold',
      },

}))


export default function SocialNetworkCard(){
    const classes = useStyles();

    return(
        <div>
            <div className={classes.row}>
                  <LinkedInIcon className={classes.rowContentText} />
                  <p className={classes.rowContentIcon}>LinkedIn</p>
                </div>
                <div className={classes.row}>
                  <FacebookIcon className={classes.rowContentText} />
                  <p className={classes.rowContentIcon}>Facebook</p>
                </div>
        </div>
    )
}