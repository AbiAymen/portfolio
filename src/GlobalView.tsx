import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { Box, Tabs, Tab, Divider } from "@material-ui/core";
import ProfilCard from "./ProfilCard";
import PersonIcon from '@material-ui/icons/Person';
import HomeIcon from '@material-ui/icons/Home';
import CodeIcon from '@material-ui/icons/Code';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import SocialNetworkCard from "./SocialNetworkCard";
import HomeTab from './HomeTab';
import Bg from './background'
import ParticlesBg from "particles-bg";
import { motion } from "framer-motion";

const drawerWidth = 240;

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
      background: "linear-gradient(to right bottom,#4467F6,#ffffff00)",
    },
    drawerContainer: {
      overflow: "auto",
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    tabRoot: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
      display: "flex",
      height: 224,
    },
    tabs: {
      borderRight: `1px solid ${theme.palette.divider}`,
    },
    row: {
      width: "100%",
      display: "flex",
      justifyContent: "flex-start",
    },

    rowContentText: {
      color: "white",
      marginTop: "auto",
      marginBottom: "auto",
      justifyContent: "left",
      marginLeft: "18%",
    },
    rowContentIcon: {
      color: "white",
      marginTop: "auto",
      marginBottom: "auto",
      marginLeft: "15%",
      justifyContent: "left",
      fontWeight: 'bold',
    },
  })
);

export default function ClippedDrawer() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
       <ParticlesBg type="circle" bg={true} />
      <CssBaseline />

      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Toolbar />
        <motion.div
  animate={{ y: 10 }}
  transition={{ ease: "easeOut", duration: 2 }}
><div className={classes.drawerContainer}>
          <ProfilCard />
          <Divider />
          <Tabs
            orientation="vertical"
            variant="scrollable"
            value={value}
            onChange={handleChange}
            aria-label="Vertical tabs example"
            className={classes.tabs}
            textColor="secondary"
          >
            <Tab
              label={
                <div className={classes.row}>
                  <HomeIcon className={classes.rowContentText} />
                  <Typography variant='body2' className={classes.rowContentIcon}>Home</Typography>
                </div>
              }
              {...a11yProps(0)}
            />
            <Tab
              label={
                <div className={classes.row}>
                  <PersonIcon className={classes.rowContentText} />
                  <Typography variant='body2' className={classes.rowContentIcon}>About me</Typography>
                </div>
              }
              {...a11yProps(1)}
            />
            <Tab
              label={
                <div className={classes.row}>
                  <CodeIcon className={classes.rowContentText} />
                  <Typography variant='body2' className={classes.rowContentIcon}>Skills</Typography>
                </div>
              }
              {...a11yProps(2)}
            />
            <Tab
              label={
                <div className={classes.row}>
                  <ContactMailIcon className={classes.rowContentText} />
                  <Typography variant='body2' className={classes.rowContentIcon}>Contact</Typography>
                </div>
              }
              {...a11yProps(3)}
            />
          </Tabs>
          <Divider />
          <Toolbar/>
          <SocialNetworkCard/>
        </div></motion.div>
       
      </Drawer>
     
      <main className={classes.content}>
        <Toolbar />
        <TabPanel value={value} index={0}>
          <HomeTab/>
        </TabPanel>
        <TabPanel value={value} index={1}>
         <Bg />
        </TabPanel>
        <TabPanel value={value} index={2}>
          Item Three
        </TabPanel>
        <TabPanel value={value} index={3}>
          Item Four
        </TabPanel>
        <TabPanel value={value} index={4}>
          Item Five
        </TabPanel>
        <TabPanel value={value} index={5}>
          Item Six
        </TabPanel>
        <TabPanel value={value} index={6}>
          Item Seven
        </TabPanel>
      </main>
    </div>
  );
}
