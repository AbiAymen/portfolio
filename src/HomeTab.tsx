import * as React from "react";
import { motion } from "framer-motion";
import { Button, Toolbar, Typography } from "@material-ui/core";

const container = {
  hidden: { opacity: 1, scale: 0 },
  visible: {
    opacity: 1,
    scale: 1,
    transition: {
      delay: 0.3,
      when: "beforeChildren",
      staggerChildren: 0.1,
    },
  },
};

const item = {
  hidden: { y: 20, opacity: 0 },
  visible: {
    y: 0,
    opacity: 1,
  },
};

export default function HomeTab() {
  return (
    <div>
      <div style={{ display: "flex", justifyContent: "flex-start" }}>
        <motion.div
          className="container"
          variants={container}
          initial="hidden"
          animate="visible"
        >
          <Typography variant="h1" color="textPrimary">
            Hello !
          </Typography>
        </motion.div>
      </div>
      <div style={{ display: "flex", justifyContent: "flex-start" }}>
        <motion.ul
          className="container"
          variants={container}
          initial="hidden"
          animate="visible"
        >
          <motion.div className="item" variants={item}>
            <Typography variant="h3" color="textPrimary">
              I'm Mohamed Aymen Abidi
            </Typography>
          </motion.div>
          <motion.div className="item" variants={item}>
            <Typography variant="h5" color="textPrimary">
              Full stack developer
            </Typography>
          </motion.div>
          <motion.div className="item" variants={item}>
            <Toolbar />
          </motion.div>
          <motion.div className="item" variants={item}>
            <Button variant="outlined" color="secondary">
              Contact
            </Button>
          </motion.div>
        </motion.ul>
      </div>
    </div>
  );
}
