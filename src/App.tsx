import React from 'react';
import './App.css';
import GlobalView from './GlobalView'
import { ThemeProvider, createMuiTheme } from '@material-ui/core';
import createPalette from '@material-ui/core/styles/createPalette';
import { blue} from '@material-ui/core/colors';


const theme = createMuiTheme({
  palette: createPalette({
    type: "light",
    primary: blue,
    secondary: {
      main : "#ffffff"
    },
    text: {
      secondary: "#ffffff",
    },
  }),
});

function App() {
  return (
    <div className="App">
    <ThemeProvider theme={theme}>
      <GlobalView />
    </ThemeProvider>
    </div>
  );
}

export default App;
